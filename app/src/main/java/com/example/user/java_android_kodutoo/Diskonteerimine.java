package com.example.user.java_android_kodutoo;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Arrays;

/**
 * Klass, mis lubab kasutajal arvutada välja
 * kupongvõlakirja väärtust
 *
 * @author Joosep
 */
public class Diskonteerimine extends AppCompatActivity {

    String sharedPreferences;
    //nupud
    private Button arvutaDiskonteerimine;
    private Button clearFields;
    private Button buttonHistory;
    //tekstiväljad
    private EditText aKupongitasu; //aastane kupongitasu v6i protsent
    private EditText nimivaartus; //nimiväärtus
    private EditText intressimaar; //väärtpaberi intressimäär või keskmine intress
    private EditText aPeriood; //aastate periood
    //tekstivaade vastuse jaoks
    private TextView vastuseTekst;
    private CheckBox checkBoxProtsent; //märkeruut


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diskonteerimine);


        aKupongitasu = (EditText) findViewById(R.id.aastaneKupongitasu);
        nimivaartus = (EditText) findViewById(R.id.nimivaartus);
        intressimaar = (EditText) findViewById(R.id.intressiMaar);
        aPeriood = (EditText) findViewById(R.id.aastatePeriood);
        vastuseTekst = (TextView) findViewById(R.id.vastuseTekst);
        checkBoxProtsent = (CheckBox) findViewById(R.id.checkBoxProtsent);
        arvutaDiskonteerimine = (Button) findViewById(R.id.arvutaDiskonteerimine);
        clearFields = (Button) findViewById(R.id.clearFields);
        buttonHistory = (Button) findViewById(R.id.buttonHistory);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        arvutaDiskonteerimine.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    //kui on tühjad, siis viskab veateate
                    if (TextUtils.isEmpty(aKupongitasu.getText().toString())) {
                        aKupongitasu.setError("Lahter peab olema täidetud!");
                    } if (TextUtils.isEmpty(nimivaartus.getText().toString())) {
                        nimivaartus.setError("Lahter peab olema täidetud!");
                    } if (TextUtils.isEmpty(intressimaar.getText().toString())) {
                        intressimaar.setError("Lahter peab olema täidetud!");
                    } if (TextUtils.isEmpty(aPeriood.getText().toString())) {
                        aPeriood.setError("Lahter peab olema täidetud!");

                        //kui märkeruut on valitud, siis võtab sisestatud väärtuse protsendina
                }else if (checkBoxProtsent.isChecked()) {
                    try {
                        Float.valueOf((aKupongitasu.getText().toString()));
                    } catch (java.lang.NumberFormatException e) {
                        aKupongitasu.setError("Vigane arv leitud!");
                        aKupongitasu.setText("");
                    }
                    try {
                        Float.valueOf((nimivaartus.getText().toString()));
                    } catch (java.lang.NumberFormatException e) {
                        nimivaartus.setError("Vigane arv leitud!");
                        nimivaartus.setText("");
                    }
                    try {
                        Float.valueOf((intressimaar.getText().toString()));
                    } catch (java.lang.NumberFormatException e) {
                        intressimaar.setError("Vigane arv leitud!");
                        intressimaar.setText("");
                    }
                    try {
                        Float.valueOf((aPeriood.getText().toString()));
                    } catch (java.lang.NumberFormatException e) {
                        aPeriood.setError("Vigane arv leitud!");
                        aPeriood.setText("");
                    }
                    float vastus = (vaartpaberiT2naneHind(
                            Float.valueOf(aKupongitasu.getText().toString()),     //aastane kupongitasuprotsent %
                            0,     //aastane kupongitasu
                            Float.valueOf(nimivaartus.getText().toString()),     //nimiväärtus
                            Float.valueOf(intressimaar.getText().toString()),     //keskmine intressimäär
                            Float.valueOf(aPeriood.getText().toString())));    //aastate periood
                    vastuseTekst.setText("Vastus: " + vastus);
                } else {
                    try {
                        Float.valueOf((aKupongitasu.getText().toString()));
                    } catch (java.lang.NumberFormatException e) {
                        aKupongitasu.setError("Vigane arv leitud!");
                        aKupongitasu.setText("");
                    }
                    try {
                        Float.valueOf((nimivaartus.getText().toString()));
                    } catch (java.lang.NumberFormatException e) {
                        nimivaartus.setError("Vigane arv leitud!");
                        nimivaartus.setText("");
                    }
                    try {
                        Float.valueOf((intressimaar.getText().toString()));
                    } catch (java.lang.NumberFormatException e) {
                        intressimaar.setError("Vigane arv leitud!");
                        intressimaar.setText("");
                    }
                    try {
                        Float.valueOf((aPeriood.getText().toString()));
                    } catch (java.lang.NumberFormatException e) {
                        aPeriood.setError("Vigane arv leitud!");
                        aPeriood.setText("");
                    }
                    float vastus = (vaartpaberiT2naneHind(
                            0,     //aastane kupongitasuprotsent %
                            Float.valueOf(aKupongitasu.getText().toString()),
                            Float.valueOf(nimivaartus.getText().toString()),
                            Float.valueOf(intressimaar.getText().toString()),
                            Float.valueOf(aPeriood.getText().toString())));
                    vastuseTekst.setText("Vastus: " + vastus);
                }
            }

            catch(java.lang.NumberFormatException e) {}
        }

    }

    );

    //clearFields nupp tühendab kõik väljad
    clearFields.setOnClickListener(new View.OnClickListener()
    {
        public void onClick (View view){
        aKupongitasu.setText("");
        nimivaartus.setText("");
        intressimaar.setText("");
        aPeriood.setText("");
    }
    }

    );
    //avab ajaloo
    buttonHistory.setOnClickListener(new View.OnClickListener()

    {
        public void onClick (View view){
        Intent myIntent = new Intent(Diskonteerimine.this, History.class);
        myIntent.putExtra("key", "DiskonteerimineVastused"); //annab kaasa parameetri, millist ajalugu vaadata
        Diskonteerimine.this.startActivity(myIntent);
    }
    }

    );
}

    /**
     * @param aastaneKupongitasuProtsent aastane kupongitasuprotsent
     * @param aastaneKupongitasu         aastane kupongitasu
     * @param nimivaartus                sisetatud nimiväärtus
     * @param keskmIntressimaar          sisestatud Keskmine intressimäär
     * @param aastatePeriood             sisestatud aastate periood
     * @return tagastab väärtpaberi tänase hinna
     */
    private float vaartpaberiT2naneHind(
            float aastaneKupongitasuProtsent,
            float aastaneKupongitasu,
            float nimivaartus,
            float keskmIntressimaar,
            float aastatePeriood) {

        float n = aastatePeriood;
        float C = aastaneKupongitasu;
        float C_protsent = aastaneKupongitasuProtsent;
        Float r = keskmIntressimaar; // või väärtpaberi intressimäär
        float F = nimivaartus;

        if (C_protsent != 0) {
            // kui protsent ON määratud, arvutab aastase kupongitasu
            // vastasel juhul pole see vajalik
            C = (C_protsent * F) / 100;
        }
        r = r / 100;

        //teeb esimese arvutuse
        float vastus = (C / (1 + r));

        //teeb teised arvutused kuni n-1
        for (int x = 2; x <= n - 1; x++) {
            double temp = 1 + r;
            double temp2 = Math.pow(temp, x);

            double tempvastus = C / temp2;
            vastus = (float) (vastus + tempvastus);

        }

        //arvutab pikka valemit sammude kaupa
        System.out.println(vastus);
        float samm1 = (C + F);
        float samm2 = 1 + r;
        float samm3 = (float) (Math.pow(samm2, n));
        float samm4 = samm1 / samm3;
        vastus = (vastus + samm4);

        addToSharedDiskonteerimine(vastus);

        return vastus; //kupongvõlakirja tänane väärtus
    }

    /**
     * Meetod, mis võtab sisendiks vastuse ja lisab Diskonteeriminevastused tulpa
     * XML failis.
     *
     * @param sisendvastus vastus, mida tahetakse salvestada
     */
    private void addToSharedDiskonteerimine(float sisendvastus) {
        String[] history; //tühi massiiv

        SharedPreferences shared = getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();

        String stringToBeConverted = shared.getString("DiskonteerimineVastused", "");
        history = MainActivity.convertStringToArray(stringToBeConverted);

        //Lisab uue koha massiivi
        history = Arrays.copyOf(history, history.length + 1);
        //Sätib järgmise koha massiivis vastuseks koos kellaaja ja kuupäevaga
        history[history.length - 1] = MainActivity.getTime() + "" + sisendvastus;
        //Salvestab massiivi XML faili android süsteemis.
        editor.putString("DiskonteerimineVastused", Arrays.toString(history));
        editor.commit();

    }

}
