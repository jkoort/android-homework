package com.example.user.java_android_kodutoo;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import java.util.Arrays;
/**
 * Settings klass lubab kasutajal muuta kuupäeva keelt
 * Samuti saab kasutaja kustutada kõik vastuste ajaloo.
 *
 * @author Joosep
 */
public class Settings extends AppCompatActivity {
    //nupp
    private Button clearHistory;
    //lüliti
    private Switch keeleLüliti;
    //String tüüpi muutujad
    private String sharedPreferences;
    private static String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        clearHistory = (Button) findViewById(R.id.button);
        keeleLüliti = (Switch) findViewById(R.id.switch1);

        SharedPreferences shared = getSharedPreferences("Keel", Context.MODE_PRIVATE);

        language = shared.getString("language", "");

        if (language.equals("English")) {
            keeleLüliti.setChecked(true);
            keeleLüliti.setText("English");

        } else if (language.equals("Estonian")) {
            keeleLüliti.setChecked(false);
            keeleLüliti.setText("Estonian");

        }

        //peale clearHistory nupu vajutamist tühjendatakse kõik ajalugu
        clearHistory.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String[] HistoryItems = new String[]{};
                Arrays.fill(HistoryItems, "");
                SharedPreferences sharedPreferencesToClear = getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferencesToClear.edit();
                editor.putString("rahaTulevikuvaartus", "");
                editor.putString("DiskonteerimineVastused", "");
                editor.putString("reaalneRahapakkumine", "");
                editor.commit();
            }
        });
        keeleLüliti.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    SharedPreferences.Editor editor = getSharedPreferences("Keel", MODE_PRIVATE).edit();
                    editor.putString("language", "English");
                    editor.commit();
                    replaceLanguage("rahaTulevikuvaartus", "Estonian", "English");
                    replaceLanguage("DiskonteerimineVastused", "Estonian", "English");
                    replaceLanguage("reaalneRahapakkumine", "Estonian", "English");
                    keeleLüliti.setText("English");

                    setResult(RESULT_OK, new Intent());

                } else {
                    SharedPreferences.Editor editor = getSharedPreferences("Keel", MODE_PRIVATE).edit();
                    editor.putString("language", "Estonian");
                    editor.commit();
                    replaceLanguage("rahaTulevikuvaartus", "English", "Estonian");
                    replaceLanguage("DiskonteerimineVastused", "English", "Estonian");
                    replaceLanguage("reaalneRahapakkumine", "English", "Estonian");
                    keeleLüliti.setText("Estonian");

                    setResult(RESULT_OK, new Intent());

                }

            }
        });
    }

    /**
     * @param stringToBeTranslated String tüüpi muutuja, mis on XML failis, määrab ära,
     * kas salvestatakse õigesse kohta.
     * @param languageTranslatedFrom Keel, mida tahetakse tõlkida
     * @param desiredLanguage Keel, milleks tahetakse tõlkida
     */
    private void replaceLanguage(String stringToBeTranslated, String languageTranslatedFrom, String desiredLanguage) {
        SharedPreferences shared = getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();

        String temporaryItems = shared.getString(stringToBeTranslated, "");
        if (languageTranslatedFrom.equals("English") && desiredLanguage.equals("Estonian")) {
            temporaryItems = temporaryItems.replaceAll("Monday", "Esmaspäev");
            temporaryItems = temporaryItems.replaceAll("Tuesday", "Teisipäev");
            temporaryItems = temporaryItems.replaceAll("Wednesday", "Kolmapäev");
            temporaryItems = temporaryItems.replaceAll("Thursday", "Neljapäev");
            temporaryItems = temporaryItems.replaceAll("Friday", "Reede");
            temporaryItems = temporaryItems.replaceAll("Saturday", "Laupäev");
            temporaryItems = temporaryItems.replaceAll("Sunday", "Pühapäev");
        } else if (languageTranslatedFrom.equals("Estonian") && desiredLanguage.equals("English")) {
            temporaryItems = temporaryItems.replaceAll("Esmaspäev", "Monday");
            temporaryItems = temporaryItems.replaceAll("Teisipäev", "Tuesday");
            temporaryItems = temporaryItems.replaceAll("Kolmapäev", "Wednesday");
            temporaryItems = temporaryItems.replaceAll("Neljapäev", "Thursday");
            temporaryItems = temporaryItems.replaceAll("Reede", "Friday");
            temporaryItems = temporaryItems.replaceAll("Laupäev", "Saturday");
            temporaryItems = temporaryItems.replaceAll("Pühapäev", "Sunday");

        }
        editor.putString(stringToBeTranslated, temporaryItems);
        editor.commit();
    }

}

