package com.example.user.java_android_kodutoo;


import android.app.ListActivity;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Klass vastuste ajaloost, mis võtab massiivi
 * salvestatud failist ning kuvab selle listina.
 *
 * @author Joosep
 */
public class History extends ListActivity {
    private String historyItems[];
    private static String sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Salvestab teksti String tüüpi muutujasse, mis saadi uue klassi alustamisega kaasa
        String SP_SAVED_ID = getIntent().getExtras().get("key").toString();

        SharedPreferences shared = getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE);

        //võtab teksti salvestatud failist
        String StringTobeConverted = shared.getString(SP_SAVED_ID, "");

        //Kutsub konverteerimise meetodi peaklassist ja määrab massiiviks.
        historyItems = MainActivity.convertStringToArray(StringTobeConverted);

        setListAdapter(new ArrayAdapter<>(this, R.layout.activity_history, historyItems));
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                //Kopeerib peale klõpstatud vastuse.
                ClipboardManager clipboard;
                clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                clipboard.setText(((TextView) view).getText());
                //Annab kasutajale teada, et vastus on kopeeritud.
                Toast.makeText(getApplicationContext(), ((TextView) view).getText() + " has been copied to clipboard!",
                        Toast.LENGTH_LONG).show();
            }
        });

    }
}