package com.example.user.java_android_kodutoo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Arrays;

/**
 * Klass, mis arvutab Raha tulevikuväärtust.
 *
 * @author Joosep
 */
public class RahaTulevikuvaartus extends AppCompatActivity {
    private static String[] history = new String[0];
    private static String sharedPreferences;
    //tekstiväljad
    private EditText algsumma;
    private EditText aastatePeriood;
    private EditText intressimaar;
    //nupud
    private Button arvutaButton;
    private Button ajaluguButton;
    //märkeruut
    private CheckBox checkBox;
    //tekstivaade
    private TextView vastusTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_raha_tulevikuvaartus);


        algsumma = (EditText) findViewById(R.id.editText);
        aastatePeriood = (EditText) findViewById(R.id.editText2);
        intressimaar = (EditText) findViewById(R.id.editText9);
        arvutaButton = (Button) findViewById(R.id.button5);
        ajaluguButton = (Button) findViewById(R.id.button6);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        vastusTextView = (TextView) findViewById(R.id.textView6);

        //avab ajaloo
        ajaluguButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(RahaTulevikuvaartus.this, History.class);
                myIntent.putExtra("key", "rahaTulevikuvaartus"); //annab kaasa parameetri
                RahaTulevikuvaartus.this.startActivity(myIntent);
            }

        });

        arvutaButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {

                    if (TextUtils.isEmpty(algsumma.getText().toString())) {
                        algsumma.setError("Ei tohi olla tühi!");
                    }
                    if (TextUtils.isEmpty(aastatePeriood.getText().toString())) {
                        aastatePeriood.setError("Ei tohi olla tühi!");
                    }
                    if (TextUtils.isEmpty(intressimaar.getText().toString())) {
                        intressimaar.setError("Ei tohi olla tühi!");

                    } else if (checkBox.isChecked()) {
                        try {
                            Float.valueOf((algsumma.getText().toString()));
                        } catch (java.lang.NumberFormatException e) {
                            algsumma.setError("Vigane arv leitud!");
                            algsumma.setText("");
                        }
                        try {
                            Float.valueOf((aastatePeriood.getText().toString()));
                        } catch (java.lang.NumberFormatException e) {
                            aastatePeriood.setError("Vigane arv leitud!");
                            aastatePeriood.setText("");
                        }
                        try {
                            Float.valueOf((intressimaar.getText().toString()));
                        } catch (java.lang.NumberFormatException e) {
                            intressimaar.setError("Vigane arv leitud!");
                            intressimaar.setText("");
                        }
                        float vastus = rahaTulevikuVaartus(
                                Float.valueOf(algsumma.getText().toString()),
                                Float.valueOf(aastatePeriood.getText().toString()),
                                0,
                                Float.valueOf(intressimaar.getText().toString())
                        );
                        vastusTextView.setText("Vastus: " + vastus);
                        //lisab vastuse XML faili.
                        addToSharedRahaTulevikuvaartus(vastus);

                    } else if (!checkBox.isChecked()) {
                        try {
                            Float.valueOf((algsumma.getText().toString()));
                        } catch (java.lang.NumberFormatException e) {
                            algsumma.setError("Vigane arv leitud!");
                            algsumma.setText("");
                        }
                        try {
                            Float.valueOf((aastatePeriood.getText().toString()));
                        } catch (java.lang.NumberFormatException e) {
                            aastatePeriood.setError("Vigane arv leitud!");
                            aastatePeriood.setText("");
                        }
                        try {
                            Float.valueOf((intressimaar.getText().toString()));
                        } catch (java.lang.NumberFormatException e) {
                            intressimaar.setError("Vigane arv leitud!");
                            intressimaar.setText("");
                        }
                        float vastus = rahaTulevikuVaartus(
                                Float.valueOf(algsumma.getText().toString()),
                                0,
                                Float.valueOf(aastatePeriood.getText().toString()),
                                Float.valueOf(intressimaar.getText().toString())
                        );
                        vastusTextView.setText("Vastus: " + vastus);
                        //lisab vastuse XML faili.
                        addToSharedRahaTulevikuvaartus(vastus);

                    }
                } catch (java.lang.NumberFormatException e) {
                }
            }

        });
    }

    /**
     * Meetod, mis arvutab raha tuleviku väärtuse
     * @param algsumma algsumma
     * @param intressimääraProtsent Juhul, kui märkeruut on valitud
     * @param intressimäär Juhul, kui märkeruut ei ole valitud
     * @param aastatePeriood aastate periood
     * @return tagastab raha tuleviku väärtuse
     */
    private float rahaTulevikuVaartus(float algsumma, float intressimääraProtsent, float intressimäär, float aastatePeriood) {
        if (intressimääraProtsent != 0) {
            intressimäär = intressimääraProtsent / 100;
        }

        //PV (Present value) algsumma
        float PV = algsumma;
        //FV (Future value) raha tuleviku väärtus
        double FV = PV * (Math.pow((1 + intressimäär), aastatePeriood));
        float vastus = (float) FV;
        return vastus;
    }

    /**
     * Meetod, mis salvestab vastuse androidi failisüsteemi
     * raha tuleviku väärtuse tulpa XML failis
     * @param sisendvastus on sisestusparameeter, mille väärtust tahetakse salvestada
     */
    private void addToSharedRahaTulevikuvaartus(float sisendvastus) {
        SharedPreferences shared = getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();

        String stringToBeConverted = shared.getString("rahaTulevikuvaartus", "");
        history = MainActivity.convertStringToArray(stringToBeConverted);

        //Lisab uue koha massiivi
        history = Arrays.copyOf(history, history.length + 1);
        //Sätib järgmise koha massiivis vastuseks koos kellaaja ja kuupäevaga
        history[history.length - 1] = MainActivity.getTime() + "" + sisendvastus;
        //Salvestab massiivi XML faili android süsteemis.
        editor.putString("rahaTulevikuvaartus", Arrays.toString(history));
        editor.commit();

    }
}
