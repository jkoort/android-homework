package com.example.user.java_android_kodutoo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * MainActivity kuvab 3 nuppu, mille peal vajutades
 * on võimalik avada uued aknad.
 * Pärast akende avamist on võimalik erinevaid asju arvutada
 * ja tulemus salvestatakse XML faili.
 * Vastuste ajalugu on võimalik näha {@link History}
 *
 * @author Joosep
 * @see History
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Nupud
     */
    private Button reaalneRahapakkumine;
    private Button diskonteerimineButton;
    private Button futureValueButton;
    /**
     * String tüüpi muutuja
     */
    public static String language;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /** Leiab nupud XML failist */
        reaalneRahapakkumine = (Button) findViewById(R.id.realRahaButton);
        diskonteerimineButton = (Button) findViewById(R.id.diskonteerimineButton);
        futureValueButton = (Button) findViewById(R.id.rahaFVButton);


        SharedPreferences shared = getSharedPreferences("Keel", Context.MODE_PRIVATE);

        //Sätib algväärtuse
        language = "English";
        //Kui keel on määratud XML failis, kasutab hoopis seda
        language = shared.getString("language", "");

        //Nupu allavajutamisel avab uue tegevuse uuel XML failil.
        reaalneRahapakkumine.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, ReaalneRahapakkumine.class);
                MainActivity.this.startActivity(myIntent);
            }
        });

        diskonteerimineButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Intent myIntent = new Intent(MainActivity.this, Diskonteerimine.class);
                MainActivity.this.startActivity(myIntent);
            }
        });

        futureValueButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Intent myIntent = new Intent(MainActivity.this, RahaTulevikuvaartus.class);
                MainActivity.this.startActivity(myIntent);
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * meetod, mis pärast keele muutmist settings klassis,
     * teeb praegusele klassile taaskäivituse,
     * muutmaks keelt.
     *
     * @param requestCode kood, mis sai määratud uue klassi käivitamisel
     * @param resultCode  resultaat
     * @param data        andmed, mis saadi kaasa
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                finish();
                startActivity(getIntent());
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent myIntent = new Intent(MainActivity.this, Settings.class);
            myIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            //Käivitab settings klassi ja annab kaasa parameetri, millele ootab vastust
            MainActivity.this.startActivityForResult(myIntent, 1);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Meetod mis eemaldab sisestatud tekstist [ ning ] ja muudab
     * massiiviks
     *
     * @param a sisestustekst
     * @return tagastab massiivi
     */
    public static String[] convertStringToArray(String a) {
        if (a.contains("[") && a.contains("]")) {
            a = a.replace("[", "");
            a = a.replace("]", "");

        }

        //jaotab vastused ära, eraldab komaga
        String historyItems[] = a.split(", ");

        if (historyItems[0].equals(",") || (historyItems[0].equals(""))) {
            //kui esimene element massiivis on tühi, eemaldab selle,
            //kuvamaks elemente õigel positsioonil
            historyItems = Arrays.copyOfRange(historyItems, 1, historyItems.length);

        }
        return historyItems;
    }

    /**
     * Meetod, mis saab teada aja ning tagastab aja tekstina sellises formaadis:
     * "Neljapäev 17.12.19"
     *
     * @return
     */
    public static String getTime() {
        Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("GMT+2"));
        int hours = cal.getTime().getHours();
        int minutes = cal.getTime().getMinutes();
        int seconds = cal.getTime().getSeconds();
        int date = cal.getTime().getDate();
        int month = cal.getTime().getMonth();
        int year = Calendar.getInstance().get(Calendar.YEAR);

        String minutesString = "" + minutes;
        String hoursString = "" + hours;
        String secondString = "" + seconds;


        String dateString = "" + date + "." + month + "." + year;
        if (minutes < 10) {
            minutesString = "0" + minutes;
        }
        if (hours < 10) {
            hoursString = "0" + hours;
        }
        if (seconds < 10) {
            secondString = "0" + seconds;
        }
        int day = cal.getTime().getDay();

        //if language isnt specified, uses just date: day, month, year
        //if language IS specified shows only day
        String dayString;
        dayString = dateString;
        if (language.equals("English")) {
            switch (day) {
                case 1:
                    dayString = "Monday";
                    break;
                case 2:
                    dayString = "Tuesday";
                    break;
                case 3:
                    dayString = "Wednesday";
                    break;
                case 4:
                    dayString = "Thursday";
                    break;
                case 5:
                    dayString = "Friday";
                    break;
                case 6:
                    dayString = "Saturday";
                    break;
                case 0:
                    dayString = "Sunday";
                    break;
            }
        } else if (language.equals("Estonian")) {

            switch (day) {
                case 1:
                    dayString = "Esmaspäev";
                    break;
                case 2:
                    dayString = "Teisipäev";
                    break;
                case 3:
                    dayString = "Kolmapäev";
                    break;
                case 4:
                    dayString = "Neljapäev";
                    break;
                case 5:
                    dayString = "Reede";
                    break;
                case 6:
                    dayString = "Laupäev";
                    break;
                case 0:
                    dayString = "Pühapäev";
                    break;
            }
        }


        String printDayAndTime = (dayString + " " + hoursString + ":" + minutesString + ":" + secondString + ", ");

        return printDayAndTime;

    }


}
