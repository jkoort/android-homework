package com.example.user.java_android_kodutoo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Arrays;

/**
 * Klass kus kasutaja saab arvutada reaalset rahapakkumist
 *
 * @author Joosep
 */
public class ReaalneRahapakkumine extends AppCompatActivity {
    //loob massiivi
    private static String[] history = new String[0];

    private EditText Lahter1;
    private EditText Lahter2;
    private Button Arvuta;
    private Button historyButton;

    private TextView Vastus;
    private static String sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realpakkumine);

        Lahter1 = (EditText) findViewById(R.id.editText3);
        Lahter2 = (EditText) findViewById(R.id.editText4);
        Vastus = (TextView) findViewById(R.id.textView4);
        Arvuta = (Button) (findViewById(R.id.arvuta));
        historyButton = (Button) findViewById(R.id.historyButton);


        Arvuta.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences shared = getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = shared.edit();
                if (TextUtils.isEmpty(Lahter1.getText().toString())) {
                    Lahter1.setError("Lahter peab olema täidetud!");

                }
                if (TextUtils.isEmpty(Lahter2.getText().toString())) {
                    Lahter2.setError("Lahter peab olema täidetud!");

                }

                //kui kumbki lahter ei ole tühi, arvutab sisestatud väärtuste põhjal reaalse rahapakkumise
                if (!TextUtils.isEmpty(Lahter1.getText().toString()) && !TextUtils.isEmpty(Lahter2.getText().toString())) {
                    try {
                        Float.valueOf((Lahter1.getText().toString()));
                    } catch (java.lang.NumberFormatException e) {
                        Lahter1.setError("Vigane arv leitud!");
                        Lahter1.setText("");
                    }
                    try {
                        Float.valueOf((Lahter2.getText().toString()));
                    } catch (java.lang.NumberFormatException e) {
                        Lahter2.setError("Vigane arv leitud!");
                        Lahter2.setText("");
                    }
                    try {
                        float nompakkumine_arv = Float.valueOf((Lahter1.getText().toString()));
                        float hinnaindeks_arv = Float.valueOf(Lahter2.getText().toString());
                        //kasutab meetodit
                        float reaalnePakkumine = ArvutaReaalnePakkumine(nompakkumine_arv, hinnaindeks_arv);
                        //määrab vastuse
                        Vastus.setText("Vastus: " + reaalnePakkumine);
                        //lisab vastuse XML faili.
                        addToSharedreaalneRahapakkumine(reaalnePakkumine);

                    } catch (java.lang.NumberFormatException e) {

                    }
                }

            }

        });


        historyButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(ReaalneRahapakkumine.this, History.class);
                myIntent.putExtra("key", "reaalneRahapakkumine"); //annab kaasa parameetri
                ReaalneRahapakkumine.this.startActivity(myIntent);
            }
        });


    }

    /**
     * Meetod, mis sisendiks võtab nominaalse pakkumise ning hinnaindeksi ning
     * neid kasutades arvutab välja reaalse rahapakkumise mis ka tagastatakse
     * Valem näeb välja reaalne pakkumine= nominaalne pakkumine/ hinnaindeks
     * @param nomPakkumine nominaalne rahapakkumine
     * @param hinnaIndeks  hinnaindeks
     * @return tagastab reaalse rahapakkumise
     */
    public float ArvutaReaalnePakkumine(float nomPakkumine, float hinnaIndeks) {
        float ReaalnePakkumine = nomPakkumine / hinnaIndeks;
        return ReaalnePakkumine;
    }

    /**
     * Meetod, mis salvestab vastuse androidi failisüsteemi
     * @param sisendvastus on sisestusparameeter, mille väärtust tahetakse salvestada
     */
    private void addToSharedreaalneRahapakkumine(float sisendvastus) {
        SharedPreferences shared = getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();

        String stringToBeConverted = shared.getString("reaalneRahapakkumine", "");
        history = MainActivity.convertStringToArray(stringToBeConverted);

        //Lisab uue koha massiivi
        history = Arrays.copyOf(history, history.length + 1);
        //Sätib järgmise koha massiivis vastuseks koos kellaaja ja kuupäevaga
        history[history.length - 1] = MainActivity.getTime() + "" + sisendvastus;
        //Salvestab massiivi XML faili android süsteemis.
        editor.putString("reaalneRahapakkumine", Arrays.toString(history));
        editor.commit();

    }


}
